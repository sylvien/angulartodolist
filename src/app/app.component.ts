import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'To Do List';
  items = [];

  ngOnInit(): void {
    this.items.push(
      {
        nom: "todo1", 
        description: "ljsdhflidshl", 
        priorite: "urgent", 
        etat: false
      }, 
      {
        nom: "todo2",
        description: "sdh ujxwh kjgxc", 
        priorite: "moyen", 
        etat: true
      });
  }
}
