import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Task } from '../task';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  nom = "";
  description: "";
  priorite;
  state;

  constructor() { }

  ngOnInit() {
  }

  @Output() add: EventEmitter<Task> = new EventEmitter<Task>()

  
  create() {
    const task = new Task();
    task.nom = this.nom;
    task.description = this.description;
    task.priorite = this.priorite;
    task.state = this.state;

    this.add.emit(task);
  }
}
