export class Task {
    nom: string;
    description: string;
    priorite: boolean;
    state: "En cours" | "Finis" | "Non commencé";
}