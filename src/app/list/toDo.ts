export class toDo {
    nom: string;
    description: string;
    priorite: string;
    etat: boolean;

    constructor(monNom: string, maDescription: string, maPriorite: string, monEtat: boolean){
        this.nom = monNom;
        this.description = maDescription;
        this.priorite = maPriorite;
        this.etat = monEtat;
    }

}