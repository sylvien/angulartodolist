import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @Input() data: Array<any>
  @Output() delete: EventEmitter<any> = new EventEmitter<any>()

  constructor() { }
  ngOnInit() {
  }

  editTask(item: any){
    console.log("edit: APPUYE")
    item.nom = "modified";
  }
  
  deleteTask(item: any){
    console.log("delete: APPUYE")
    console.log(item);
    this.delete.emit(item);
  }

}
